package ph.enmis.cardbored.gl;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by nmcapule on 1/15/15.
 */
public class CardGLSurfaceView extends GLSurfaceView {

    public CardGLSurfaceView(Context context) {
        super(context);

        setEGLContextClientVersion(2);

        setRenderer(new CardGLRenderer());
        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }
}
