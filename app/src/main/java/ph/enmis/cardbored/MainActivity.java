package ph.enmis.cardbored;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

import ph.enmis.cardbored.gl.CardGLSurfaceView;

public class MainActivity extends Activity {
    GLSurfaceView mGLView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGLView = new CardGLSurfaceView(this);
        setContentView(mGLView);
    }
}
